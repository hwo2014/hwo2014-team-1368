#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>

#include "cJSON.h"

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *switch_left();
static cJSON *switch_right();
static cJSON *make_msg(char *type, cJSON *msg);

static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        puts("Someone crashed");
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
}

int main(int argc, char *argv[])
{
	//FILE *fp;
	
    int sock;
    cJSON *json, *konstanta;

    if (argc != 5)
        error("Usage: bot host port botname botkey\n");

    sock = connect_to(argv[1], argv[2]);

    json = join_msg(argv[3], argv[4]);
    write_msg(sock, json);
    cJSON_Delete(json);
	int usek = 0,i_1=0,pamat=0;
	int mapa_i[100];
	double mapa_d[5][100];
	//double vahy[1000];
	//double sumy[1000];
	//double skryte[15];
	//double vstup[30];
	//char subor[30];
	double uhol=0, uholH=0;
/*	fp = fopen("vahy.txt","r");
	if (fp!=NULL)
	do{
		fscanf(fp,"%lf",&vahy[i_1]);
//		printf("%lf\t%d\n",vahy[i_1],i_1);
		i_1++;
	}
	while(i_1<300);

	printf("i_1 = %d\n",i_1);

	fclose(fp);
//	printf("fclose ok");
*/  
while ((json = read_msg(sock)) != NULL) {
        cJSON *msg, *msg_type, *msg_data, *race, *track, *pieces, *jazdec, *id, *piecePosition, *pieceIndex, *kandidat, *car, *millis, *name, *result;
        char *msg_type_name, *msg_data_name, *msg_usek, *msg_name_data;
	int jazdci = 0, poloha = 0, i,j,k;
	double rychlost = 0;
	
//	printf("%s",cJSON_Print(json));	//vypis vsetkych JSON-ov

        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");
        msg_type_name = msg_type->valuestring;
	if (!strcmp("gameInit", msg_type_name)) {
		msg_data = cJSON_GetObjectItem(json, "data");
		race = cJSON_GetObjectItem(msg_data, "race");
		track = cJSON_GetObjectItem(race, "track");
		pieces = cJSON_GetObjectItem(track, "pieces");
		
		usek = cJSON_GetArraySize(pieces);
//		printf("%s\n%d\n",cJSON_PrintUnformatted(pieces),usek);
		for (i=0;i<usek;i++){
			mapa_i[i] = cJSON_GetArraySize(cJSON_GetArrayItem(pieces,i));
			mapa_d[0][i]=mapa_d[1][i]=mapa_d[2][i]=mapa_d[3][i]=0;
			for (j=0;j<(int)mapa_i[i];j++){
				msg_name_data = (cJSON_GetArrayItem(cJSON_GetArrayItem(pieces,i),j)->string);
//				printf("%s\n",msg_name_data);
				if(!strcmp("length", msg_name_data)) 
					mapa_d[0][i]=atof(cJSON_Print(cJSON_GetArrayItem(cJSON_GetArrayItem(pieces,i),j)));
				if(!strcmp("switch", msg_name_data))
				//	if("true"==cJSON_Print(cJSON_GetArrayItem(cJSON_GetArrayItem(pieces,i),j)))
						mapa_d[1][i]=1;
				else
						mapa_d[1][i]=0;
				
				if(!strcmp("radius", msg_name_data))
					mapa_d[2][i]=atof(cJSON_Print(cJSON_GetArrayItem(cJSON_GetArrayItem(pieces,i),j)));
				if(!strcmp("angle", msg_name_data))
					mapa_d[3][i]=atof(cJSON_Print(cJSON_GetArrayItem(cJSON_GetArrayItem(pieces,i),j)));
				mapa_d[4][i]=0;			
		}
		}
	}

	if(!strcmp("gameEnd", msg_type_name)){
		msg_data = cJSON_GetObjectItem(json, "data");
		msg_data = cJSON_GetObjectItem(msg_data, "results");
		jazdci = cJSON_GetArraySize(msg_data);
//	printf("OK 1\n");
		for(k=0;k<jazdci;k++){
		jazdec = cJSON_GetArrayItem(msg_data, k);
//	printf("OK 2\n%s\n",cJSON_Print(jazdec));
		car = cJSON_GetObjectItem(jazdec, "car");
//	printf("OK 3\n%s\n",cJSON_Print(car));
		name = cJSON_GetObjectItem(car, "name");
//	printf("OK 4\n%s\n",cJSON_Print(name));	
		result = cJSON_GetObjectItem(jazdec, "result");
//	printf("OK 5\n%s\n",cJSON_Print(result));
		millis = cJSON_GetObjectItem(result, "millis");
//	printf("OK 6\n%s\n",cJSON_Print(millis));
		msg_data_name = name->valuestring;
//	printf("OK 7\n%s\n",msg_data_name);
		if(!strcmp("www.janicko.sk", msg_data_name)){
			printf("celkovy cas bol int %d",millis->valueint);
//			sprintf(subor,"vahy_%d_.txt",millis->valueint);
//			fp = fopen(subor,"w");
//			if (fp==NULL)printf("chyba\n");
		//	printf("cekovy cas bol string %s",millis->valuestring);
		//printf("OK\n");
		}
	}}

        if (!strcmp("carPositions", msg_type_name)) {
		msg_data = cJSON_GetObjectItem(json, "data");
		jazdci = cJSON_GetArraySize(msg_data);
		for(k=0;k<jazdci;k++){
		jazdec = cJSON_GetArrayItem(msg_data, k);
		id = cJSON_GetObjectItem(jazdec, "id");
		name = cJSON_GetObjectItem(id, "name");
		msg_data_name = name->valuestring;
		if(!strcmp("www.janicko.sk", msg_data_name)){
			piecePosition = cJSON_GetObjectItem(jazdec, "piecePosition");
			pieceIndex = cJSON_GetObjectItem(piecePosition, "pieceIndex");
			poloha = atoi(cJSON_Print(pieceIndex));
			kandidat = cJSON_GetObjectItem(jazdec, "angle");
		//	printf("USEK %lf \t %s\n",kandidat->valuedouble, kandidat);
			uhol = abs(kandidat->valuedouble);
	
			rychlost = 1;//0.645;
		//	if ((mapa_d[0][poloha]>0)&&(mapa_d[0][poloha+1]>0)) rychlost = 1;
			if ((uhol>5)&&(uhol<50)&&(uhol<=uholH)) rychlost = 1;
			if ((mapa_d[0][poloha]>0)&&(mapa_d[0][poloha+1]==0)) rychlost = -1+(mapa_d[2][poloha+1]/102);
			if (mapa_d[0][poloha]==0) rychlost = 0.75*(mapa_d[2][poloha]/100);
	//		if ((uhol>5)&&(uhol<50)&&(uhol<=uholH)) rychlost = 1;
			if ((uhol>5)&&(uhol>uholH)) rychlost = 0;

			
			uholH=uhol;
			if (rychlost>1) rychlost = 1;
			if (rychlost<=0) rychlost = 0;
		//	if (poloha<3) rychlost = 1;
			if (poloha>(usek-2))	rychlost = 1;

		//}

	//		printf("USEK %d \t %d \t %g %g %g %g : \t%g\t --- \t%g\n",poloha, mapa_i[poloha], mapa_d[0][poloha], mapa_d[1][poloha], mapa_d[2][poloha], mapa_d[3][poloha], rychlost ,uhol);
			msg = throttle_msg(rychlost);
			if ((pamat==0)&&(mapa_d[1][poloha+1]==1))
			{
				if((mapa_d[3][poloha+1]+mapa_d[3][poloha+2]+mapa_d[3][poloha+3]+mapa_d[3][poloha+4]+mapa_d[3][poloha+5]+mapa_d[3][poloha+6])<(-1))
					msg = switch_left();
				if((mapa_d[3][poloha+1]+mapa_d[3][poloha+2]+mapa_d[3][poloha+3]+mapa_d[3][poloha+4]+mapa_d[3][poloha+5]+mapa_d[3][poloha+6])>(1))
					msg = switch_right();
				pamat = 1;
			}
			if (mapa_d[1][poloha+1]==0)
				pamat = 0;
//			kandidat = NULL;
		}
		}

        } else {
            log_message(msg_type_name, json);
            msg = ping_msg();
	    
        }

        write_msg(sock, msg);

        cJSON_Delete(msg);
        cJSON_Delete(json);
    }
/*
    for(i_1=0;i_1<300;i_1++)
	    fprintf(fp,"%lf\n",vahy[i_1]);
    fclose(fp);
*/

    return 0;
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *switch_left()
{
	return make_msg("switchLane", cJSON_CreateString("Left"));
}
static cJSON *switch_right()
{
	return make_msg("switchLane", cJSON_CreateString("Right"));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}
